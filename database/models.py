from mongoengine import *
from enum import Enum


class WorkTypeEnum(Enum):
    much_contact = 0
    medium_contact = 1
    little_contact = 2
    no_contact = 3


class TestedStatusEnum(Enum):
    positive = 0
    negative = 1
    not_tested = 2


class UserRoleEnum(Enum):
    admin = 0  # admins can upload hospital submitions with amount of new cases
    user = 1  # can submit surveys


# ====== Document Collections

class User(Document):
    login_name = StringField()
    password = StringField()
    role_type = IntField()


class SurveyAnswer(Document):
    date_submitted = DateField()
    work_type =  IntField()
    tested_status = IntField()
    user = ReferenceField(User)

    age = IntField()
    postal_code = StringField()
    feeling_sick = BooleanField()
    contact_to_infected_person = BooleanField()
    fever = FloatField()
    cough = StringField()#switch to enum?
    has_sniffles = BooleanField()
    has_sore_throat = BooleanField()
    has_chills = BooleanField()
    has_breathing_difficulties = BooleanField()
    has_stomach_pain = BooleanField()
    days_since_symptoms_started = IntField()
    has_chronic_respiratory_diseases = BooleanField()
    is_smoker = BooleanField()
    has_cardiovascular_diseases = BooleanField()
    has_immunodefficiency = BooleanField()
    has_obesity = BooleanField()

    result = FloatField()


class HospitalSubmition(Document):
    date_submitted = DateField()
    postal_code = StringField()
    hospital_name = StringField()
    amount_tested_positive = IntField()
    amount_recovered = IntField()
