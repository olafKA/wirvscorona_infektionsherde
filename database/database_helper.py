from database.models import *


class DatabaseHelper:
    '''Class for accessing objects, lists of filtered objects, etc. from database  '''

    def __init__(self, db_connection):
        self.db = db_connection

    @staticmethod
    def find_users_all():
        try:
            users = User.objects(role_type=UserRoleEnum.user).all()
            return users
        except DoesNotExist:
            return None

    @staticmethod
    def find_admins_all():
        try:
            users = User.objects(role_type=UserRoleEnum.admin).all()
            return users
        except DoesNotExist:
            return None

    @staticmethod
    def find_surveys(query_params):
        try:
            surveys = SurveyAnswer.objects(__raw__=query_params).all()
            return surveys
        except DoesNotExist:
            return None
    #
    # @staticmethod
    # def add_hospital_submition(query_params):
    #
