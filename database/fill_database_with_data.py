from database.database_helper import *
from database.models import *
import datetime

class DatabaseFiller:
    '''Used to fill database with data from surveys and hospitals'''

    def __init__(self, db):
        global database_helper
        database_helper = DatabaseHelper(db)
        self.db = db

    @staticmethod
    def add_user(username, password, role: UserRoleEnum):
        return User(login_name=username, password=password, role_type=role.value).save()

    @staticmethod
    def save_survey_answer(answer_dict,user):
        survey_answer = SurveyAnswer(age=answer_dict['age'], date_submitted=datetime.date.today(),
        tested_status = answer_dict['tested'] ,
        postal_code = str(answer_dict['postcode']) ,
        feeling_sick = answer_dict['sick'] ,
        contact_to_infected_person = answer_dict['contact_to_infected_persons'] ,
        fever = answer_dict['fever'] ,
        cough = answer_dict['cough'] ,
        has_sniffles = answer_dict['sniff'] ,
        has_sore_throat = answer_dict['sire_throat'] ,
        has_chills = answer_dict['chills'] ,
        has_breathing_difficulties = answer_dict['breath_difficulties'] ,
        has_stomach_pain = answer_dict['stomach_pain'] ,
        days_since_symptoms_started = answer_dict['days_since_symptoms'] ,
        has_chronic_respiratory_diseases = answer_dict['respiratory_diseases'] ,
        is_smoker = answer_dict['smoker'] ,
        has_cardiovascular_diseases = answer_dict['cariovascular_diseases'] ,
        has_immunodefficiency = answer_dict['immunodeficiency'] ,
        has_obesity = answer_dict['obesity'] ,
        work_type=0, user= user).save()





if __name__ == "__main__":
    db_connection = connect(db='infektionsherde', host='localhost:27017', port=27017)
    database_helper = DatabaseHelper(db_connection)
    filler = DatabaseFiller(db_connection)
    import pandas as pd
    data = pd.read_csv("E:/Hackathon/wirvscorona_infektionsherde/user_test_data.csv")
    for i,row in data.iterrows():
        if i!=0:
            user=filler.add_user(row['user'],"pass", role=UserRoleEnum.user)
            filler.save_survey_answer(row,user)
