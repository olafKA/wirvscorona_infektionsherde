import requests
import pandas as pd
import plotly.express as px
import numpy as np
from scipy.optimize import curve_fit
import plotly.graph_objects as go
from geopy.geocoders import Nominatim
import tqdm
from ratelimit import limits, sleep_and_retry
from ipywidgets import interactive, HBox, VBox

@sleep_and_retry
@limits(1,1)

class Visualizer:

    def __init__(self, url='https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson'):
        self.url = url
        self.get_data()

    def get_data(self):
        """
        :return: pandas dataframe with all entries of the RKI data
        """

        r = requests.get(url=self.url)
        jsonData = r.json()
        content = []
        for i, features in enumerate(jsonData['features']):
            content.append(features['properties'])
        self.df = pd.DataFrame(content)
        self.df['Meldedatum'] = pd.to_datetime(self.df['Meldedatum'])
        self.df.to_csv('landkreisdaten.csv', index=False)
        # print all counties
        #print('\n'.join(list(dict.fromkeys(self.df['Landkreis']))))
        #print('Bundesländer')
        #print('\n'.join(list(dict.fromkeys(self.df['Bundesland']))))



    def create_history_diagram(self, html_name="history-plot.html", area='SK Hamburg', log=False, area_type='Landkreis', stats_type='AnzahlFall'):
        """
        open plotly line diagram, visualization of historical data of a single county

        :param area:
        :param log:
        :param area_type:
        :param stats_type:
        :return: None
        """

        title = 'Fallzahlen' if stats_type == 'AnzahlFall' else 'AnzahlTodesfall'
        if (log):
            title = title + '(logarithmisch)'
            type = 'log'
        else:
            type = 'linear'
            title = title

        if area_type == 'Landkreis':
            areaData = self.df[area == self.df['Landkreis']]
        elif area_type == 'Bundesland':
            areaData = self.df[area == self.df['Bundesland']]
        else:
            return

        cumStateData = pd.DataFrame(areaData.groupby(areaData['Meldedatum'])[stats_type].sum().cumsum())
        cumStateData.reset_index(inplace=True)

        #areaData = self.df['SK Karlsruhe' == self.df['Landkreis']]
        #stateData = pd.DataFrame(areaData.groupby(areaData['Meldedatum'])[stats_type].sum().cumsum())
        #stateData.reset_index(inplace=True)
        #scluster0 = px.line(stateData, x='Meldedatum', y=stats_type)

        fig = px.line(cumStateData, x='Meldedatum', y=stats_type)

        fig.update_layout(
            title=("Zeitlicher Verlauf der Fallzahlen " + area),
            xaxis_title="Meldedatum",
            yaxis_title=title,
            font=dict(
                family="Courier New, monospace",
                size=18,
                color="#7f7f7f"
            ),
            updatemenus=[  # Sollten wir noch einen Titel hinzufügen?
                dict(
                    buttons=list([
                        dict(label="Linear", method="relayout", args=[{"yaxis": {"type": "linear"}}]),
                        dict(label="Log", method="relayout", args=[{"yaxis": {"type": "log"}}]),
                    ],),
                )]
        )

        fig.write_html(html_name)

        # open plotly server to display result
        fig.show()

    def plot_by_ages(self, html_name="age_groups.html"):
        """

        :return:
        """
        group_by_age = self.df.groupby(by=['Altersgruppe', 'Geschlecht']).count()
        males = []
        females = []
        unknowns = []
        age_groups = self.df.Altersgruppe.unique()

        for i in range(len(age_groups)):
            males.append(group_by_age.loc[age_groups[i], 'M'][0])
            females.append(group_by_age.loc[age_groups[i], 'W'][0])
            unknowns.append(group_by_age.loc[age_groups[i], 'unbekannt'][0])

        data = []
        data.extend([go.Bar(name='M', x=age_groups, y=males),
                     go.Bar(name='F', x=age_groups, y=females),
                     go.Bar(name='unbekannt', x=age_groups, y=unknowns)])
        fig = go.Figure(data=data)
        fig.update_layout(barmode='stack')
        fig.write_html(html_name)
        fig.show()

    def map_to_html(self, html_name='map.html', csv_file='rkiData.csv', plot_type='scatter'):
        gc = Nominatim(user_agent="wir-vs-corona-python", timeout=3)

        try:
            countyDf = pd.read_csv(csv_file)
            countyDf['lat']
        except:

            print(f"Getting the current data.")

            data_df = {'GEN': [], 'Infections': []}
            for county in self.df['Landkreis'].drop_duplicates():
                collect_county_num = self.df[self.df['Landkreis'] == county]
                num_county = collect_county_num.loc[:, 'AnzahlFall'].cumsum()
                num_county = num_county.loc[num_county.index[-1]]
                if 'SK' in county or 'LK' in county:
                    name = county[3:]
                elif 'Region' in county:
                    name = county[7:]
                    if 'gion' in name:
                        name = name[5:]
                else:
                    print(f"Not specified - {county}")
                data_df['GEN'].append(name)
                data_df['Infections'].append(num_county)

            countyDf = pd.DataFrame(data_df)

            print(f"Converting the data to geo location coordinates...")

            coordinates = dict(lat=np.zeros((len(countyDf))), long=np.zeros((len(countyDf))))
            for idx, row in tqdm.tqdm(countyDf.iterrows()):
                _, (lat, long) = gc.geocode(row["GEN"])
                coordinates['lat'][idx] = lat
                coordinates['long'][idx] = long

            countyDf['lat'] = coordinates['lat']
            countyDf['long'] = coordinates['long']
            print(f"Finished conversion.")

        countyDf.to_csv('rkiData.csv', index=False)

        invalid_data = countyDf[(countyDf['GEN'] == 'Bergstraße') | (countyDf['GEN'] == 'Friesland') | \
                                (countyDf['GEN'] == 'Schaumburg')]

        countyDf.drop(invalid_data.index, inplace=True)
        if plot_type == 'scatter':
            fig = px.scatter_mapbox(countyDf, lat=countyDf['lat'], lon=countyDf['long'], hover_name='GEN', hover_data=['Infections',], #'Infectionsper1000population'],
                                size='Infections',  zoom=5, mapbox_style='carto-positron',
                                    labels={'Infections': 'Infektionen', 'Infectionsper1000population': 'Infektionen / 1000 Einwohner',})
                                           #'lat': 'l', 'long': 'l'}) #color='Infectionsper1000population',
        elif plot_type == 'heatmap':
            fig = px.density_mapbox(countyDf, lat='lat', lon='long', hover_name='GEN', hover_data=['Infections'],
                                    radius=20, zoom=5, mapbox_style='carto-positron')
        else:
            raise NotImplementedError(f"Not implemented.")

        fig.write_html(html_name)
        fig.show()

def main():

    # API
    visualizer = Visualizer(url='https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson')
    # example execution: Call function for generation
    visualizer.create_history_diagram(html_name='history_hamburg.html', area='SK Hamburg', log=False, area_type='Landkreis', stats_type='AnzahlFall')
    visualizer.create_history_diagram(html_name='history_bayern.html', area='Bayern', log=False, area_type='Bundesland', stats_type='AnzahlFall')
    #visualizer.create_history_diagram(html_name='history_tote_bayern.html', area='Bayern', log=False, area_type='Bundesland', stats_type='AnzahlTodesfall')
    #visualizer.plot_by_ages(html_name="age_groups.html")
    visualizer.map_to_html(html_name='map_of_countries.html', csv_file='rkiData.csv', plot_type='scatter')

if __name__ == '__main__':

    main()
