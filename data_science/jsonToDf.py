import pandas as pd
import json

with open('test.json') as jsonFile:
    jsonData = json.load(jsonFile)

columns = []
for field in jsonData['fields']:
    columns.append(field['name'])
data = []
for feature in jsonData['features']:
    data.append(feature['attributes'])

df = pd.DataFrame(data=data, columns=columns)
