import requests
import json
import pandas as pd
from urllib.request import urlopen
from pandas.io.json import json_normalize
url='https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson'
json_url = urlopen(url)
data = json.loads(json_url.read())
feats=data['features']
df_de = pd.DataFrame(columns=['GEN', 'death_rate', 'cases', 'deaths', 'cases_per_population', 'cases_per_100k'])
i=0
for location in feats:
    attr_loc=location['attributes']
    df_loc = pd.DataFrame(attr_loc, index=[i])
    i=i+1
    df_de=df_de.append(df_loc)
print(df_de.to_string())
##Es wird ein Dataframe df_de erstellt, in dem die unterschiedlichen Punkte für die Landkreise eingespeichert sind