import { Component } from '@angular/core';
import {TestState} from '../components/building-blocks/entry-card/entry-card.component';

@Component({
    selector: 'app-hospital',
    templateUrl: './hospital.component.html',
    styleUrls: ['./hospital.component.scss']
})
export class HospitalComponent {

    /**
     * Mock data for test entries.
     */
    readonly mockTestEntryData: Array<TestEntryData> = [
        {id: '1645', state: 'pending'},
        {id: '4523', state: 'positive'},
        {id: '3567', state: 'untested'},
        {id: '4445', state: 'untested'},
        {id: '4565', state: 'negative'},
        {id: '6893', state: 'pending'},
        {id: '5734', state: 'positive'},
        {id: '4568', state: 'positive'},
        {id: '3495', state: 'untested'},
        {id: '2310', state: 'positive'},
        {id: '1154', state: 'negative'},
        {id: '1426', state: 'positive'},
        {id: '1431', state: 'positive'},
        {id: '4234', state: 'untested'},
        {id: '1545', state: 'pending'},
        {id: '1337', state: 'negative'},
        {id: '4144', state: 'positive'},
        {id: '9415', state: 'pending'}
    ];
}

export interface TestEntryData {
    id: string;
    state: TestState;
}
