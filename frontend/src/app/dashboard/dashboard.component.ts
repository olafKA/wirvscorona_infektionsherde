import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    states: any = [
        {cases: 3807, deaths: 21, name: 'Baden-Württemberg'},
        {cases: 3650, deaths: 21, name: 'Bayern'},
        {cases: 3545, deaths: 6, name: 'Nordrhein-Westfalen'},
        {cases: 1306, deaths: 1, name: 'Niedersachsen'},
        {cases: 1175, deaths: 2, name: 'Hessen'},
        {cases: 1053, deaths: 2, name: 'Rheinland-Pfalz'},
        {cases: 1024, deaths: 1, name: 'Berlin'},
        {cases: 872, deaths: 0, name: 'Hamburg'},
        {cases: 606, deaths: 0, name: 'Sachsen'},
        {cases: 347, deaths: 1, name: 'Schleswig-Holstein'},
        {cases: 274, deaths: 0, name: 'Brandenburg'},
        {cases: 216, deaths: 0, name: 'Thüringen'},
        {cases: 211, deaths: 0, name: 'Sachsen-Anhalt'},
        {cases: 187, deaths: 0, name: 'Saarland'},
        {cases: 172, deaths: 0, name: 'Mecklenburg-Vorpommern'},
        {cases: 165, deaths: 0, name: 'Bremen'},
    ];
    totalCases = this.states.reduce((a, b) => a + (b.cases || 0), 0);

    constructor() {
    }

    ngOnInit() {
    }

}
