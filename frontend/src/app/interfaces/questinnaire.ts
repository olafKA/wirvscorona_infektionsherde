export interface IQuestionnair {
    testStatus: string;
    age: number;
    alreadySymptoms: boolean;
    generalCondition: string;
    contactCovid: boolean;
    contactPotentialCovid: boolean;
    fever: number;
    feverSince: number;
    cough: string;
    lostTast: boolean;
    sniff: boolean;
    stomachPain: boolean;
    breathingProblem: boolean;
    shortBreath: boolean;
    soreThroat: boolean;
    chills: boolean;
    sinceWhen: number;
    respiratoryDisease: boolean;
    smoker: boolean;
    cardiovascularDisease: boolean;
    immunocompromised: boolean;
    overweight: boolean;
    userId: string;
}
