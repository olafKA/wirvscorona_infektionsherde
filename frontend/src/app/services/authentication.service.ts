import { Injectable } from '@angular/core';
import {UserRole} from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    /**
     * Role of current user.
     */
    userRole: UserRole;
}
