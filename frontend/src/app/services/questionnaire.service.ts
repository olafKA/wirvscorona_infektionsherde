import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IQuestionnair} from '../interfaces/questinnaire';
import {of} from 'rxjs';

@Injectable()
export class QuestionnaireService {

    public result: any;

    constructor(private http: HttpClient) {
    }

    public submitDataToBackend(data: any) {

        const mappedQuestionnairData = this.mapQuestionnaireSheme(data);

        const url = '/api/prediction';

        // return this.http.post(url, mappedQuestionnairData);
        return of();
    }

    public mapQuestionnaireSheme(data: any): IQuestionnair {

        const date1 = new Date(data.seitWann).getTime();
        const date2 = new Date().getTime();
        const diffTime = Math.abs(date2 - date1);
        const sinceWhen = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        const mappedData: IQuestionnair = {
            testStatus: data.bereits_getestet ? data.test_ergebnis : 'kein Test',
            age: data.alter,
            alreadySymptoms: false,
            generalCondition: 'mittelmäßig',
            contactCovid: data.nachweislich_inf,
            contactPotentialCovid: data.moeglich_inf,
            fever: data.temperature,
            feverSince: sinceWhen,
            sinceWhen: sinceWhen,
            cough: data.husten,
            lostTast: false,
            sniff: data.schnupfen,
            stomachPain: data.bauchschmerzen,
            breathingProblem: data.atemprobleme,
            shortBreath: false,
            soreThroat: data.halsschmerzen,
            chills: data.schuettelfrost,
            respiratoryDisease: data.atemwegserkrankung,
            immunocompromised: data.immunschwaeche,
            overweight: data.switchThirteen,
            smoker: data.raucher,
            cardiovascularDisease: false,
            userId: '697d61c4f2e7471e87a502571546ea7b'
        };

        console.log('mappedDAta: ', mappedData);
        return mappedData;
    }
}
