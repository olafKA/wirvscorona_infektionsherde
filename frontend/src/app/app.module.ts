import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentsModule} from './components/components.module';
import {QuestionnaireService} from './services/questionnaire.service';
import {HttpClientModule} from '@angular/common/http';
import { ImprintComponent } from './imprint/imprint.component';

@NgModule({
    declarations: [
        AppComponent,
        ImprintComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        ComponentsModule
    ],
    providers: [],
    exports: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
