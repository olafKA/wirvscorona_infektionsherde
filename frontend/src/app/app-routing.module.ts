import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './guard/auth.guard';
import {ImprintComponent} from "./imprint/imprint.component";

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
}, {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard]
}, {
    path: 'hospital',
    loadChildren: () => import('./hospital/hospital.module').then(m => m.HospitalModule),
    canActivate: [AuthGuard]
}, {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
}, {
    path: 'questionnaire',
    loadChildren: () => import('./questionnaire/questionnaire.module').then(m => m.QuestionnaireModule),
    canActivate: [AuthGuard]
}, {
    path: 'map',
    loadChildren: () => import('./map/map.module').then(m => m.MapModule),
    canActivate: [AuthGuard]
}, {
    path: 'imprint',
    component: ImprintComponent
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
