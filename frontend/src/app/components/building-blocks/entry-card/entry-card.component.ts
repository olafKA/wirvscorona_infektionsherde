import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-entry-card',
  templateUrl: './entry-card.component.html',
  styleUrls: ['./entry-card.component.scss']
})
export class EntryCardComponent {

    /**
     * Id of test entry.
     */
   @Input() id: string;

    /**
     * Id of test entry.
     */
    @Input() state: TestState;
}

export type TestState = 'positive' |'negative' | 'untested' | 'pending';
