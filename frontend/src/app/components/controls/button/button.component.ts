import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

    /**
     * Label of button.
     */
    @Input() label: string;

    /**
     * Icon name to be displayed inside button.
     */
    @Input() iconName: string;

    /**
     * Type of button, primary or secondary, default is secondary.
     */
    @Input() type: ButtonType;

    /**
     * Whether button is disabled.
     */
    @Input() disabled: boolean;

    /**
     * Emitted when button was clicked.
     */
    @Output() wasClicked: EventEmitter<void> = new EventEmitter<void>();
}

export type ButtonType = 'primary' | 'secondary';
