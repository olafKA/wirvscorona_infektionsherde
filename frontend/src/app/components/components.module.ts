import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from './controls/button/button.component';
import {EntryCardComponent} from './building-blocks/entry-card/entry-card.component';


@NgModule({
    declarations: [
        ButtonComponent,
        EntryCardComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ButtonComponent,
        EntryCardComponent
    ]
})
export class ComponentsModule {
}
