import {Component} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {UserRole} from '../app.component';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    constructor(
        private authService: AuthenticationService,
        private router: Router
    ) {}

    /**
     * Triggers mocked application login.
     * @param role Role of user to be signed in.
     */
    signin(role: UserRole): void {
        this.authService.userRole = role;
        this.router.navigate(['dashboard']);
    }
}
