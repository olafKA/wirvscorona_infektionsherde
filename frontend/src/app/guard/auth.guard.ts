import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    roles: {[key: string]: Array<string>} = {
        hospital: ['login', 'dashboard', 'map', 'hospital'],
        patient: ['login', 'dashboard', 'map', 'questionnaire']
    };

    constructor(private authService: AuthenticationService,
                private router: Router) {}

    /**
     * Checks whether selected route can be activated.
     * @param route The route to be checked.
     */
    canActivate(route: ActivatedRouteSnapshot): boolean {
        // route to be accessed
        const routePath: string = route.url[0].path;

        if (!this.authService.userRole) {
            this.router.navigate(['login']);
            return false;
        }
        const access: boolean = !!this.roles[this.authService.userRole].find(path => path === routePath);

        if (!access) {
            this.router.navigate(['dasboard']);
        }

        return access;
    }
}
