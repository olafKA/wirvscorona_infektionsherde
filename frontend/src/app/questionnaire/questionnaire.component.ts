import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {QuestionnaireService} from '../services/questionnaire.service';
import _ from 'lodash';

@Component({
    selector: 'app-questionnaire',
    templateUrl: './questionnaire.component.html',
    styleUrls: ['./questionnaire.component.scss'],
})
export class QuestionnaireComponent implements OnInit {
    public temperature = 37.5;
    public questionnaire: FormGroup;
    public showErrorMessage: boolean;
    public questionToggles: boolean[];
    public dropDownState: boolean;

    constructor(private formBuilder: FormBuilder,
                private questionnaireService: QuestionnaireService) {
        this.showErrorMessage = false;
        this.dropDownState = false;
        this.createForm();
        this.questionToggles = [];
        _.each(this.questionnaire.controls, (value, key) => {
            this.questionToggles[key] = false;
        });
    }

    ngOnInit() {
    }

    public createForm() {
        this.questionnaire = this.formBuilder.group({
            bereits_getestet: new FormControl( '', { validators: Validators.required }),
            test_ergebnis: new FormControl( null),
            alter: new FormControl( null, Validators.required),
            plz: new FormControl( '', { validators: [
                    Validators.required,
                    Validators.minLength(5)
                ]}),
            nachweislich_inf: new FormControl( null, Validators.required),
            moeglich_inf: new FormControl( null, Validators.required),
            krank: new FormControl( null, Validators.required),
            temperature: new FormControl( null, [
                Validators.required,
                Validators.min(35),
                Validators.max(42)
            ]),
            husten: new FormControl( null, Validators.required),
            schnupfen: new FormControl( null, Validators.required),
            bauchschmerzen: new FormControl( null, Validators.required),
            atemprobleme: new FormControl( null, Validators.required),
            halsschmerzen: new FormControl( null, Validators.required),
            schuettelfrost: new FormControl( null, Validators.required),
            seitWann: new FormControl( null, Validators.required),
            atemwegserkrankung: new FormControl( null, Validators.required),
            atemwegs_welche: new FormControl( null),
            raucher: new FormControl( null, Validators.required),
            herzkreislauf: new FormControl( null, Validators.required),
            herzkreislauf_welche: new FormControl( null),
            immunschwaeche: new FormControl( null, Validators.required),
            immunschwaeche_welche: new FormControl( null),
            switchThirteen: new FormControl( null, Validators.required)
        });
    }

    public submitData($event) {
        $event.preventDefault();
        this.questionnaireService.submitDataToBackend(
            this.questionnaire.getRawValue()
        ).subscribe(this.handleSubmitQuestionnaireData);
    }

    public handleSubmitQuestionnaireData(response: any) {
        console.log('Response: ', response);
    }

    public toggleQuestion(question, dependenQuestion) {
        this.questionToggles[question] = this.questionnaire.controls[dependenQuestion].value;
    }

    public toggleDropdown() {
        this.dropDownState = !this.dropDownState;
    }
}
