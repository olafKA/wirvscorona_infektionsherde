import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {QuestionnaireRoutingModule} from './questionnaire-routing.module';
import {QuestionnaireComponent} from './questionnaire.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ComponentsModule} from '../components/components.module';
import {QuestionnaireService} from "../services/questionnaire.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
    declarations: [QuestionnaireComponent],
    providers: [
        QuestionnaireService
    ],
    imports: [
        CommonModule,
        QuestionnaireRoutingModule,
        ReactiveFormsModule,
        ComponentsModule,
        HttpClientModule
    ]
})
export class QuestionnaireModule {
}
