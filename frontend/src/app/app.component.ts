import {Component} from '@angular/core';
import {AuthenticationService} from './services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(
        private authService: AuthenticationService,
        private router: Router
    ) {}

    /**
     * Returns current user role.
     */
    get userRole(): UserRole {
        return this.authService.userRole;
    }

    /**
     * Returns current route.
     */
    get route(): string {
        return this.router.url;
    }

    /**
     * Navigates to a given rule.
     */
    navigateTo(route: string): void {
        this.router.navigate([route]);
    }

    /**
     * Triggers mocked application logout.
     */
    signout(): void {
        this.navigateTo('login');
        this.authService.userRole = undefined;
    }
}

export type UserRole = 'patient' | 'hospital';
